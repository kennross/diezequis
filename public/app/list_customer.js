$(document).ready(function () {
    let data = [];

    $.ajax({
        method: 'GET',
        url: '/api/get/customers/json',
        success: function (server) {
            $.each(server, function (index, object) {
                data.push(arrayKeys(object));
            });

            $('#dataTable').DataTable({
                searching: true,
                order: [[1, 'asc']],
                data: data,
                language: {
                    sProcessing: 'Procesando...',
                    sLengthMenu: 'Mostrar _MENU_ registros',
                    sZeroRecords: 'No se encontraron resultados',
                    sEmptyTable: 'Ningún dato disponible en esta tabla',
                    sInfo: 'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
                    sInfoEmpty: 'Mostrando registros del 0 al 0 de un total de 0 registros',
                    sInfoFiltered: '(filtrado de un total de _MAX_ registros)',
                    sInfoPostFix: '',
                    sSearch: 'Buscar:',
                    sUrl: '',
                    sInfoThousands: '',
                    sLoadingRecords: 'Cargando...',
                    oPaginate: {
                        sFirst: 'Primero',
                        sLast: 'Último',
                        sNext: 'Siguiente',
                        sPrevious: 'Anterior'
                    },
                    oAria: {
                        sSortAscending: ': Activar para ordenar la columna de manera ascendente',
                        sSortDescending: ': Activar para ordenar la columna de manera descendente'
                    }
                }
            });
        }
    });
});

function arrayKeys(myObject) {
    let array_values = [];

    for (let key in myObject) {
        array_values.push(myObject[key]);
    }

    return array_values;
}