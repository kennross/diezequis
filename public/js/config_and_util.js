/* CONFIG */

$().ready(function () {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
});

/* configure token general */
$.ajaxSetup({
    dataType: 'JSON',
    method: 'POST',
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    error: function (server, status, xhr) {
        if (server.status === 422) {
            let errors = $.parseJSON(server.responseText);
            let html = '';
            $.each(errors, function (key, value) {
                if ($.isPlainObject(value)) {
                    $.each(value, function (key, value) {
                        html += '<li>' + value + '</li>';
                    });
                }
            });

            swal({
                'type': 'warning',
                'title': 'Error de validación, detalles:',
                'text': html,
                'confirmButtonColor': '#ff7417',
                'html': true
            });

            return false;
        }

        let msg = '';
        if (server.status === 0) {
            msg = 'No estás conectado a internet, verifica tu conexión';
        } else if (server.status === 404) {
            msg = 'Recurso no encontrado [404]';
        } else if (server.status === 500) {
            msg = 'Error interno del servidor [500].';
        } else if (status === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (status === 'timeout') {
            msg = 'Sesión vencida';
        } else if (status === 'abort') {
            msg = 'Ajax request aborted.';
        } else if (status === 419) {
            msg = 'El token CSRF ha vencido, por su seguridad, la acción que está a punto de hacer, no se hará, debe recargar la página';
        } else if (status === 401) {
            swal({
                title: "¡Ups!",
                text: "Tu sesión ha caducado, vuelvo a iniciar sesión",
                type: "warning",
                showConfirmButton: true
            }, function () {
                location.href = '/login';
            });
        } else {
            msg = 'Uncaught Error.\n' + server.status;
        }

        swal({
            title: "¡Ups!",
            text: "Ocurrió un error\n" + msg,
            type: "error",
            timer: 3500,
            showConfirmButton: false
        });
    }
});

$.validate({
    lang: 'es',
    modules: 'location, date, security, file',
});

moment.locale("es");

/* UTIL */
function startLoader(type, btn, text) {
    setTimeout(function () {
        if (type == 'id') {
            $('#' + btn).html('<i class="fa fa-cog fa-spin"></i>&nbsp; ' + text);

        } else if (type == 'class') {
            $('.' + btn).html('<i class="fa fa-cog fa-spin"></i>&nbsp; ' + text);
        }
        if (type == 'id') {
            $('#' + btn).attr('disabled', true);

        } else if (type == 'class') {
            $('.' + btn).attr('disabled', true);
        }
    }, 150);
}

function finishLoader(type, btn, text) {
    if (type == 'id') {
        $('#' + btn).removeAttr('disabled');
        $('#' + btn).html(text);
    } else if (type == 'class') {
        setTimeout(function () {
            $('.' + btn).removeAttr('disabled');
            $('.' + btn).html(text);
        }, 500);
    }
}

function uploadFile(id, path_storage, success, complete) {
    var data = new FormData();
    data.append('file', document.getElementById(id).files[0]);
    data.append('path', path_storage);

    $.ajax({
        url: '/api/public/util/upload/file',
        enctype: 'multipart/form-data',
        data: data,
        cache: false,
        processData: false,
        contentType: false,
        success: function (name_asset) {
            success(name_asset);
        },
        complete: function () {
            if (complete != null) {
                complete();
            }
        }
    });
}

/* Vue Filter Predefine */
function truncate(text, stop, clamp) {
    return text != null ? (text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')) : '';
}

function date_string(value) {
    return moment(value, "YYYY-MM-DD HH:mm").format("MMM D - Y");
}

function date_only(value) {
    return moment(value, "YYYY-MM-DD").format("MMMM DD, YYYY");
}

function from_now(value) {
    return moment(value, "YYYY-MM-DD HH:mm").subtract(5, 'hours').fromNow();
}

function toFormattedDateString(value) {
    return moment(value, 'YYYY-MM-DD').format('MMM DD, YYYY');
}

Array.prototype.contains = function (obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
};

/*  JS File Upload */
$(function () {
    $('[type="file"]').change(function () {
        var label = $(this).parent().find('span');
        if (typeof(this.files) != 'undefined') {
            var file = this.files[0];
            var name = file.name;
            var size = (file.size / 1048576).toFixed(3);
            label.addClass('withFile').text(name + ' – ' + size + 'mb');
        }
        else {
            var name = this.value.split("\\");
            label.addClass('withFile').text(name[name.length - 1]);
        }
        return false;
    });
});

String.prototype.capitalize = function () {
    return this.replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
    });
};