<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Prueba para ingresar a DiezEquis

###Se evaluará
- Creatividad
- Análisis
- Desarrollo

###Tecnologías a tomar en cuenta:
- MySQL
- PHP
- Javascript / Jquery
- DataTables
- Bootstrap

###Actividades Puntuales
- Migrar un archivo CSV a una base de datos MySQL
- Obtener la información que previamente se migró. 
- Los datos deben procesarse como objetos array y convertirlos a objetos JSON e insertarlos a una Tabla a través del atributo “data”, 
- Habilitar la función de búsquedas
- Ordenar por Cliente de manera ascendente
 
- Todo debe realizarse con la librería DataTables de javascript.
- Aplicando Bootstrap, 
- Subirlo al hosting gratuito

Recuerda que tienes libertad de realizarlo a tu gusto.