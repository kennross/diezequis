<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Storage;
use DB;
use File;

class HomeController extends Controller
{
    public function init() {
        DB::unprepared(File::get(public_path() . '/storage/customers_structure.sql'));
        session()->flash('message', 'Aplicación correctamente inicializada');
        return redirect('/');
    }

    /**
     * Mostrando la vista para cargar archivos
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('home');
    }

    /**
     * Lógica para carga y manipulación de archivo
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadCsv(Request $request)
    {
        /* Primera parte: Cargar archivo */
        $path = public_path() . '/storage/csv/';

        $name = md5(microtime()) . '.' . $request->csv->getClientOriginalExtension();
        $request->file('csv')->move($path, $name);


        /* Lectura del archivo CSV */
        $handle = fopen(public_path() . '/storage/csv/' . $name, "r");


        /* Recorrer el CSV */
        $count = 0;
        while ($csvLine = fgetcsv($handle, 0, ";")) {
            $csvLine = $this->clearTrim($csvLine);
            if ($count > 0 && $this->rowValid($csvLine)) {
                Customer::create([
                    'hour' => $csvLine[0],
                    'customer_external_code' => $csvLine[1],
                    'email' => $csvLine[2],
                    'description' => $csvLine[3],
                    'old_price' => $this->convertStringToDouble($csvLine[4]),
                    'new_price' => $this->convertStringToDouble($csvLine[5])
                ]);
            }
            $count++;
        }
        fclose($handle);

        session()->flash('message', 'El archivo ha sido cargado correctamente');
        return redirect('list');
    }

    /**
     * Convertir la cadena en números válidos para el campo double
     * @param $string
     * @return mixed
     */
    private function convertStringToDouble($string)
    {
        return str_replace(',', '.', str_replace('.', '', $string));
    }

    /**
     * Validar si no hay filas con campos vacíos, para evitar error en el query
     * @param $row
     * @return bool
     */
    private function rowValid($row)
    {
        return $row[0] !== '' && $row[1] !== '' && $row[2] !== '' && $row[3] !== '' && $row[4] !== '' && $row[5] !== '';
    }

    /**
     * Limpiar los datos de espacios
     * @param $row
     * @return mixed
     */
    private function clearTrim($row)
    {
        foreach ($row as $key => $r) {
            $row[$key] = trim($r);
        }

        return $row;
    }

    /**
     * Mostrando la vista del lsitado de los clientes
     *
     * @return \Illuminate\Http\Response
     */
    public function listCustomers()
    {
        return view('customers');
    }


    /**
     * Enviar los datos de la BD a JSON a través de AJAX
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiGetCustomersJson()
    {
        return response()->json(Customer::select(['hour', 'customer_external_code', 'email', 'description', 'old_price', 'new_price'])->get());
    }
}
