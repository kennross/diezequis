<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Kennit Romero">

    <link rel="icon" href="{{ asset('favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
          integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">

    @yield('css')
</head>
<body>
<div id="app">


    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link @if(Request::is('/')) active @endif" href="{{ url('/') }}">
                        <i class="fa fa-upload"></i>
                        Cargar Archivo

                        <!-- Accesibilidad -->
                        @if(Request::is('/'))
                            <span class="sr-only">(current)</span>
                        @endif
                    </a>

                    <a class="nav-item nav-link @if(Request::is('list')) active @endif" href="{{ url('list') }}">
                        <i class="fa fa-list-alt"></i>
                        &nbsp;
                        Listado de Clientes

                        <!-- Accesibilidad -->
                        @if(Request::is('list'))
                            <span class="sr-only">(current)</span>
                        @endif
                    </a>
                </div>
            </div>
            <div class="nav navbar-nav navbar-right">

                <a href="#modal_autor" data-toggle="modal">
                    <i class="fa fa-user"></i>
                    &nbsp;
                    Sobre Autor
                </a>

            </div>
        </div>
    </nav>

    <main class="py-4">

        <div class="container">
            @if(Session::has('message'))
                <section class="content-header">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">
				&times;
			</span>
                        </button>
                        <i class="fa fa-check-circle"></i>
                        &nbsp;
                        {!!Session::get('message')!!}
                    </div>
                </section>
            @endif
        </div>

        @yield('content')
    </main>


    <div class="container">
        <div class="card footer">
            <div class="card-body">
                Todos los derechos reservados

                <i class="fa fa-copyright"></i>

                2018

                <span class="pull-right">
                    DiezEquis S.A.
                </span>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modal_autor">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sobre Autor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="media">
                        <img class="mr-3 img-responsive" style="width: 260px"
                             src="https://lh3.googleusercontent.com/Xivf_AHtQiQYoHY2Zv-dLxrIBJ06u3JU2zybPVaDxoEzLf7RtcQTTcBzrH92tEaqNVuxWX4Gk4P_q1nFjPWUGmxt7duNLO2W8xWBxUbSWoq6LdnTppZ2SujSQHTdXdUdfOSUCwgAtP0nk18H3m5ZF1IZc8uY55-0hEYf8YLr8KUMNvfCM8QUqY-OtEiYv8n7Kh7RF4UUp2sNSDX8TWd8wVZKMGZtLExBuE8mscvqFtM5JCGunTexr7kn3aGcoKtxWaLo4Vnn3sc7JjyXh5cFAwo57_qXAassH5zbunoOPW7mvYdbVFyMb6wks_MTkA3jMlp2lNfGx6PT9VFwPRUar5mz8ZJKEPrhP7TOcW4xgrC1Uf50nTG_4mKWG107ungcMTfYrsE-q1BO0X2gFSyGYguQbDrfUa16AOsg2ODNNMoj_d5e8jRYq-jP0PWmqNZsCdKgfjte8ApEM_gGrvc_WxkNF8qoFugdmQUPIr16TmfDaaGBei3m4pffC7ziAU6Qh8jb2Q5Fr6-_E4bFUO2QJ5cI1T9Mwnp_q0_medyazWMY14kHIkEHNh0CAGSdGjaZGEET1ja5Vfj9ru411L0np7E_u5J5kWpT8aBpxxe-=w743-h1006-no"
                             alt="Foto de Kennit">
                        <div class="media-body text-justify">
                            <h5 class="mt-0 text-primary">Kennit David Ruz Romero</h5>
                            Me considero una persona autodidacta, creativa, con iniciativa, emprendedor, perseverante y
                            disciplinado, capacitado o dispuesto a ser capacitado para resolver problemas o automatizar
                            procesos en muy corto tiempo.
                        </div>
                    </div>

                    <div class="margin"></div>

                    <h6>
                        Habilidades
                    </h6>
                    <ul class="nmb">
                        <li>
                            Elicitación de Requerimientos
                        </li>
                        <li>
                            Programador en lenguajes PHP y Java
                        </li>
                        <li>
                            Automatización de pruebas
                        </li>
                        <li>
                            Herramientas de gestión y comunicación
                            <ul>
                                <li>
                                    Slack
                                </li>
                                <li>
                                    Trello
                                </li>
                                <li>
                                    GitLab
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- JQuery 2.2.4 -->
<script src="https://code.jquery.com/jquery-2.2.4.js"
        integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
        crossorigin="anonymous"></script>

<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
        integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>

<!-- Form Validator -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<!-- Moment -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>

<script src="{{ asset('js/config_and_util.js?v=0.0.1') }}"></script>

@yield('js')
</body>
</html>
