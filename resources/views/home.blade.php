@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Migración CSV
                    </div>

                    <div class="card-body">

                        <div class="alert alert-dark" role="alert">
                            El archivo CSV debe estar separado por <code>;</code> y se debe tener presente que la
                            primera fila es para emcabezados.
                        </div>


                        <form action="{{ url('upload/csv') }}" enctype="multipart/form-data" method="POST">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label for="csv">
                                    CSV
                                </label>
                                <span class="fit button-file">
                                    <input type="file" class="form-control" name="csv" id="csv" data-validation="mime size required"
                                           data-validation-max-size="2M" data-validation-allowing="csv">
                                    <span>Clic para abrir gestor de archivos</span></span>
                            </div>

                            <div class="text-center">
                                <button class="btn btn-success">
                                    <i class="fa fa-upload"></i>
                                    &nbsp;
                                    Cargar Datos
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
