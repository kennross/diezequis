@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="container">
        <div class="justify-content-center">
            <div class="card">
                <div class="card-header">
                    Listado de Clientes
                </div>

                <div class="card-body">
                    <table class="table table-striped table-bordered nmb" id="dataTable">
                        <thead>
                        <tr>
                            <th>Hora</th>
                            <th>Cliente</th>
                            <th>Correo Electrónico</th>
                            <th>Producto</th>
                            <th>
                                <i class="fa fa-dollar"></i>
                                &nbsp;
                                Viejo
                            </th>
                            <th>
                                <i class="fa fa-dollar"></i>
                                &nbsp;
                                Nuevo
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('app/list_customer.js?v=0.0.1') }}"></script>
@endsection
